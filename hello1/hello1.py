"""
Data Application.
"""

from kelvin.app import ApplicationConfig, DataApplication


class AppConfig(ApplicationConfig):
    """Application Config."""

    lower: float = 0.0
    upper: float = 1000.0
    count: float = 0.0
    print("Message AppConfig - Application Initialized")

class App(DataApplication):
    """Application."""

    config: AppConfig



    TOPICS = {
        "#.*": {
            "target": "history.{name}",
            "storage_type": "buffer",
            "storage_config": {"window": {"minutes": 10}, "getter": "value"},
        }
    }
    CHECKS = {
        "pressure": {
            "max_lag": {"seconds": 5},
        },
    }
    LIMITS = {
        "doubled_pressure": {
            "frequency": {"seconds": 0.5},
        },
    }

    def process(self) -> None:
        """Process data."""

        pressure = self.data.pressure

        data_status = self.data_status
        if data_status:
            self.logger.warning("Missing Data", data_status=data_status)

            # pressure = self.data.pressure
            self.config.count = self.config.count + 2

            print("Missed ", self.config.count)
            print("pressure: ", pressure.value)
            print("data status: ", data_status)
            return

        # pressure = self.data.pressure

        if self.config.lower <= pressure.value <= self.config.upper:
            doubled_pressure = self.make_message(
                "raw.float32",
                "doubled_pressure",
                time_of_validity=pressure._.time_of_validity,
                value=2.0 * pressure.value,
            )
            self.emit(doubled_pressure)

        else:
            print("Pressure Bad: ", pressure)
            history = self.data.history.pressure.series()
            self.logger.debug("pressure", n=len(history), mean=history.mean(), std=history.std())